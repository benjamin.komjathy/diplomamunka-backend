from app.routers.root import root_router
from app.routers.user import user_router
from app.routers.todo import todo_router

from app.functions.functions import *

app = FastAPI(title="Diplomamunka-backend",root_path="/api")

app.include_router(root_router)
app.include_router(user_router)
app.include_router(todo_router)


origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.on_event("startup")
async def startup():
    if not database.is_connected:
        await database.connect()
    # create some dummy entry
    #await dummies()

@app.on_event("shutdown")
async def shutdown():
    if database.is_connected:
        await database.disconnect()




