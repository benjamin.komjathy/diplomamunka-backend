import time
from typing import Dict

import jwt
from decouple import config


JWT_SECRET = "2545e7a49f79d0701d2a9584c01835bc35601521f05c98d5a9792f0bd2e18bd3"
JWT_ALGORITHM = "HS256"


def token_response(token: str):
    return {
        "access_token": token,
        "expires": time.time() + 60000
    }

def signJWT(id:int,email: str) -> Dict[str, str]:
    payload = {
        "id":id,
        "email": email,
        "expires": time.time() + 60000
    }
    token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)

    return token_response(token)

def registerJWT(email: str,pw:str, name:str) -> Dict[str, str]:
    payload = {

        "email": email,
        "pw": pw,
        "name":name,

        "expires": time.time() + 86400
    }
    token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)

    return token_response(token)

def decodeJWT(token: str) -> dict:
    try:
        decoded_token = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
        return decoded_token if decoded_token["expires"] >= time.time() else None
    except:
        return {}
