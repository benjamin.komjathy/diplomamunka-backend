from app.functions.functions import *

todo_router = APIRouter(
    tags=['Todo'],
    prefix="/todo",
   # dependencies=[Depends(JWTBearer())]
)

@todo_router.get("/")
async def read_todo(token = Depends(JWTBearer())):
    payload = decodeJWT(token)
    thisUser = await User.objects.get(id=payload['id'])
    alltodos = await Todo.objects.order_by(Todo.due.asc()).all(user_id=thisUser.id)
    alltodos = jsonable_encoder(alltodos)
    for i in alltodos:
        if i['user_id']:
            i['user_id'] = i['user_id']['id']
    return alltodos



@todo_router.post("/", status_code=status.HTTP_201_CREATED)
async def create_todo(todo: Todo,token: str = Depends(JWTBearer())):
    payload = decodeJWT(token)

    #data=await Todo.objects.all(user_id=payload['id'])
    #data=jsonable_encoder(data)
    #for i in data:
    #    if i["todo"]==todo.todo:
    #        raise HTTPException(status_code=400, detail="this todo already exist")
    try:
        todo.user_id=payload['id']
        return await todo.save()
    except:
        raise HTTPException(status_code=404, detail="something went wrong")

@todo_router.get("/{id}")
async def get_todo(id: int,token: str = Depends(JWTBearer())):
    payload = decodeJWT(token)
    thisUser = await User.objects.get(id=payload['id'])

    data = await Todo.objects.all(user_id=thisUser.pk)
    data = jsonable_encoder(data)
    todo_ids=usersTodos(data)

    if id not in todo_ids:
        raise HTTPException(status_code=400, detail="this todo belongs to another user")
    try:
        thisTodo = await Todo.objects.get(id=id)

        return thisTodo
    except ormar.NoMatch:
        raise HTTPException(status_code=404, detail="id not found")

@todo_router.put("/{id}")
async def modify_todo(id: int, todo: TodoUpdate,token: str = Depends(JWTBearer())):
    payload = decodeJWT(token)
    thisUser = await User.objects.get(id=payload['id'])

    data = await Todo.objects.all(user_id=thisUser.pk)
    data = jsonable_encoder(data)
    todo_ids=usersTodos(data)

    if id not in todo_ids:
        raise HTTPException(status_code=400, detail="this todo belongs to another user")
    try:
        thisTodo = await Todo.objects.get(id=id)

        if todo.todo:

            #for i in data:
            #    if i["todo"] == todo.todo:
            #        raise HTTPException(status_code=400, detail="a todo with this todo already exist")
            thisTodo.todo = todo.todo
        
        if todo.due:
            thisTodo.due = todo.due
        
        thisTodo.sync_at=round(datetime.timestamp(datetime.now()))
            
        # if todo.user_id:
        #     thisUser = await User.objects.get(id=todo.user_id)
        #     thisTodo.user_id = thisUser
        await thisTodo.update()
        return await Todo.objects.get(id=id)
    except ormar.NoMatch:
        raise HTTPException(status_code=404, detail="id not found")

@todo_router.delete("/{id}")
async def delete_todo(id: int,token: str = Depends(JWTBearer())):
    
    await todobelongstouser(id, token)
    try:
        deletedtodo = await Todo.objects.get(id=id)
        
        
        await deletedtodo.delete()

        return {"deleted todo": deletedtodo}
    except ormar.NoMatch:
        raise HTTPException(status_code=404, detail="id not found")
