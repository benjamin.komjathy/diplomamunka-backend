from app.functions.functions import *


user_router = APIRouter(
    prefix="/users",
    tags=['Users']
)


@user_router.get("/")
async def read_users(token: str = Depends(JWTBearer())):
    payload = decodeJWT(token)
    if 'syscops.com' not in payload['email']:
        return {'details': "not authenticated"}
    return await User.objects.all()

@user_router.get("/me")
async def read_users(token: str = Depends(JWTBearer())):
    payload = decodeJWT(token)
    thisUser = await User.objects.get(id=payload['id'])
    return thisUser



@user_router.post("/", status_code=status.HTTP_201_CREATED)
async def create_user(user: User):
    try:
        email=user.email
        valid=validate_email(email)
        user.email = valid.email
    except:
        raise HTTPException(status_code=404, detail="bad email format")

    user.pw=get_password_hash(user.pw)

    try:
        await user.save()
        return signJWT(user.id,user.email)
    except asyncpg.exceptions.UniqueViolationError:
        raise HTTPException(status_code=404, detail="email already exist")
        # return ("Unexpected error:", sys.exc_info()[0])

@user_router.post("/login")
async def user_login(user: UserLogin):
    # print(user)
    try:
        thisUser = await User.objects.get(email=user.email)
        if verify_password(user.pw, thisUser.pw):
            content = signJWT(thisUser.pk,user.email)

            bytes = str(content['access_token'], 'utf-8')

            content = {"access_token": bytes, "expire": round(content['expires'])}
            response = JSONResponse(content=content)
            response.set_cookie(key="access_token", value=bytes)

            return response
        else:
            raise HTTPException(status_code=404, detail="bad login details")
        # try:
        #     if await User.objects.get(email=user.email):
        #         return signJWT(user.email)
    except:
        raise HTTPException(status_code=404, detail="bad login details")

@user_router.post("/signup", status_code=status.HTTP_201_CREATED)
async def signup(user: User):
    try:
        email=user.email
        valid=validate_email(email)
        user.email = valid.email
    except:
        raise HTTPException(status_code=404, detail="bad email format")
    emails=[]
    users=await User.objects.all()
    for i in users:
        # print(i)
        if i.email:
            emails.append(i.email)
    if user.email in emails:
        raise HTTPException(status_code=404, detail="email already exists")
    # print(emails)
    user.pw = get_password_hash(user.pw)
    token=registerJWT(user.email,user.pw,user.name)
    bytes = str(token['access_token'], 'utf-8')

    sg = sendgrid.SendGridAPIClient(api_key="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")
    from_email = Email("beni@syscops.com")
    to_email = To(email)
    subject = "Verify your email for TODO app"
    content = Content("text/plain", "http://localhost/users/verify?token={}".format(bytes))
    mail = Mail(from_email, to_email, subject, content)
    response = sg.client.mail.send.post(request_body=mail.get())
    # print(response.status_code)
    # print(response.body)
    # print(response.headers)
    try:

        return token
    except:
        raise HTTPException(status_code=404, detail="something went wrong")

@user_router.get("/verify", status_code=status.HTTP_201_CREATED)
async def verify(token:str):
    payload = decodeJWT(token)
    # print(payload)

    # user.pw=payload['pw']

    try:
        # user=await User(email=payload['email'], pw=pw,name=payload['name']).save()
        user = await User.objects.get_or_create(email=payload['email'], pw=payload['pw'], name=payload['name'])
        # print(user)
        token=signJWT(user.id,user.email)

        return token
    except:
        raise HTTPException(status_code=404, detail="something went wrong")




@user_router.put("/")
async def modify_user(user: UserUpdate,token: str = Depends(JWTBearer())):
    payload = decodeJWT(token)
    thisUser = await User.objects.get(id=payload['id'])
    try:

        if user.name:
            thisUser.name = user.name

        if user.email:
            try:
                email = user.email
                valid = validate_email(email)
                user.email = valid.email
            except:
                raise HTTPException(status_code=404, detail="bad email format")
            thisUser.email = user.email
        if user.pw:
            user.pw = get_password_hash(user.pw)
            thisUser.pw = user.pw

        await thisUser.update()
        return await User.objects.get(id=payload['id'])
    except ormar.NoMatch:
        raise HTTPException(status_code=404, detail="id not found")



# @user_router.get("/{id}")
# async def get_user(id: int):
#     try:
#
#         return await User.objects.get(id=id)
#     except ormar.NoMatch:
#         raise HTTPException(status_code=404, detail="Id doesnt exist")

@user_router.delete("/")
async def delete_user(token: str = Depends(JWTBearer())):
    payload = decodeJWT(token)
    
    try:

        deletedUser = await User.objects.get(id=payload['id'])
        todos = await Todo.objects.all(user_id=deletedUser.pk)
        for i in todos:
            
            await i.delete()
        await deletedUser.delete()

        # await User.objects.delete(id=id)
        return {"deleted user": deletedUser}
    except ormar.NoMatch:
        raise HTTPException(status_code=404, detail="id not found")



