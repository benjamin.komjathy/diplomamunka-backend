from app.functions.functions import *

root_router = APIRouter(
    tags=['Root'],
    #dependencies=[Depends(JWTBearer())]
)

@root_router.get("/",status_code=status.HTTP_200_OK)
async def read_root():
    return {"message": "Backend is healthy"}



@root_router.get("/db")
async def read_root(token: str = Depends(JWTBearer())):
    payload = decodeJWT(token)
    if 'syscops.com' not in payload['email']:
        return {'details':"not authenticated"}

    return await User.objects.select_all(follow=True).all()