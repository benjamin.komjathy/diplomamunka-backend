import databases
import ormar
import sqlalchemy
from sqlalchemy import func, text
from typing import Optional
from pydantic import BaseModel, EmailStr
from .config import settings
from datetime import datetime

database = databases.Database(settings.db_url)
metadata = sqlalchemy.MetaData()


class BaseMeta(ormar.ModelMeta):
    metadata = metadata
    database = database


class User(ormar.Model):
    class Meta(BaseMeta):
        tablename = "users"

    id: int = ormar.Integer(primary_key=True)
    email: str = ormar.String(max_length=128, unique=True, nullable=False)
    pw: str = ormar.String(max_length=128, nullable=False)
    name: str = ormar.String(max_length=255, nullable=False)


class UserUpdate(BaseModel):
    email: Optional[str] = ormar.String(max_length=128)
    pw: Optional[str] = ormar.String(max_length=128)
    name: Optional[str] = ormar.String(max_length=255)


class UserLogin(BaseModel):
    email: str
    pw: str



class Todo(ormar.Model):
    class Meta(BaseMeta):
        tablename = "todo"

    id: int = ormar.Integer(primary_key=True)
    user_id: int = ormar.ForeignKey(User)
    sync_at: datetime = ormar.BigInteger(nullable=False,default=round(datetime.timestamp(datetime.now())))
    todo: str = ormar.String(max_length=128, default='mindent', nullable=True)
    due: int = ormar.BigInteger(nullable=True,default=None)
    

class TodoUpdate(BaseModel):

    #id: int = ormar.Integer(primary_key=True)
    #doer: Optional[str] = ormar.String(max_length=128, nullable=False)
    todo: Optional[str] = ormar.String(max_length=128,  nullable=True)
    due: Optional[int] = ormar.BigInteger(nullable=True)
    


engine = sqlalchemy.create_engine(settings.db_url)
#
#metadata.drop_all(engine)

metadata.create_all(engine)
