from datetime import datetime, timedelta
import calendar
import copy
import ormar
from fastapi import FastAPI,APIRouter, HTTPException, status, Depends, Query
from fastapi.middleware.cors import CORSMiddleware
from typing import Optional

from app.db import database, User, UserUpdate, UserLogin,Todo,TodoUpdate
from fastapi.encoders import jsonable_encoder
from email_validator import validate_email, EmailNotValidError
from app.auth.auth_bearer import JWTBearer
from app.auth.auth_handler import signJWT, decodeJWT,registerJWT
from passlib.context import CryptContext
from fastapi.responses import JSONResponse
import sendgrid
import os
from sendgrid.helpers.mail import *
import sys
import asyncpg

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)




def usersTodos(todos):
    ls = []
    for i in todos:
        ls.append(i['id'])
    return ls

async def todobelongstouser(id,token):
    payload = decodeJWT(token)
    thisUser = await User.objects.get(id=payload['id'])

    data = await Todo.objects.all(user_id=thisUser.pk)
    data = jsonable_encoder(data)
    todo_ids = usersTodos(data)

    if id not in todo_ids:
        raise HTTPException(status_code=400, detail="this todo belongs to another user")



async def dummies():
    dummypw = (get_password_hash('96Deep15'))
    await User.objects.get_or_create(email="beni@syscops.com", pw=dummypw,name="Beni")
    
    thisuser = await User.objects.get(name="Beni")
    

    await Todo.objects.get_or_create(user_id=thisuser,todo="elkezdeni a diplomamunkát")
  

    user = await User(email="martin@syscops.com", pw=dummypw,name="Martin").save()
    user = await Todo(user_id=user,todo="segíteni Beninek a diplomamunkában").save()
   


