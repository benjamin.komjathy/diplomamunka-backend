# Diplomamunka Backend

## Informations

This project was part of my Master's thesis, what can be found here [here](https://benjaminkomjathy.hu/assets/pdf/Komj%C3%A1thy%20Benjamin%20-%20Diplomamunka.pdf) (written in hungarian)

## Live demo

https://todo.benjaminkomjathy.hu/api

## Docs

https://todo.benjaminkomjathy.hu/api/docs

# Try-out locally


```
docker-compose up -d
```